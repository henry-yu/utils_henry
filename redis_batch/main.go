package main

import (
    "strings"
    "os"
    "bufio"
	"errors"
    "fmt"
    "encoding/json"
    log "github.com/Sirupsen/logrus"
    "flag"
	"gopkg.in/redis.v3"
    "regexp"
    "strconv"
    "unicode/utf8"
    "net/url"
)

var (
    flagFile = flag.String("file", "all", "input file")
    flagOut = flag.String("out", "all", "output file")
)

type Link struct {
    ID string
    float64 string
}


type entPop struct {
	_1 string
	_2 string
}

func main() {
    flag.Parse()
    log.Println("Processing file: ", *flagFile)

/*
    rclient := redis.NewClient(&redis.Options{
        Addr: "content-proto.8w0nsl.0001.usw2.cache.amazonaws.com:6379",
        DB:   2,
    }) 
*/

    file, err := os.Open(*flagFile)
    if err != nil {
        panic(err)
    }
    defer file.Close()
/*
    outFile, err := os.Create(*flagOut)
    if err != nil {
        panic(err)
    }
    defer outFile.Close()
*/

	fmt.Println("SELECT 5")

    scanner := bufio.NewScanner(file)
//	entPopArr := make([]entPop, 0)
    for scanner.Scan() {
        if err := scanner.Err(); err != nil {
            panic(err)
        }
        line := scanner.Text() 
		//record := strings.Split(line, " ")
		//curEnt := entPop{}
		curEnt := make(map[string]interface{})
        err := json.Unmarshal([]byte(line), &curEnt)
        if err != nil {
			log.Errorln(err)
            panic(err)
        }
        fmt.Println("SET" + " \"" + curEnt["_1"].(string) + "\" " + strconv.FormatFloat(curEnt["_2"].(float64), 'f', 2, 64))
        //fmt.Println("DEL" + " \"" + curEnt["_1"].(string) + "\"")
        //fmt.Println(curEnt["_1"].(string) + " " + strconv.FormatFloat(curEnt["_2"].(float64), 'f', 2, 64))
    }

/*
    for _,v := range arr {
        json_str, err := json.Marshal(v)
        if err != nil {
            panic(err)
        }
        fmt.Println(string(json_str))
    }
*/
}

func processEntity(input string) (string, error) {
    trimmed := strings.TrimPrefix(strings.TrimSuffix(input, ">"), "<http://dbpedia.org/resource/")
	unescaped, err := url.QueryUnescape(trimmed)
	if err != nil {
		return unescaped, err
	}
	unescaped = strings.Replace(unescaped, " ", "+", -1)
	unescaped = strings.Replace(unescaped, "/", "_", -1)
	if strings.HasSuffix(unescaped, "(disambiguation)") {
		return unescaped, errors.New("Skip this entity: disambiguation page")
	}
	return unescaped, nil
}

func checkLabel(ID string, labelsClient *redis.Client) (string, error) {
   return labelsClient.Get("id:lb|en:" + "en" + "#" + ID).Result()
}

func checkAbstract(ID string, client *redis.Client) int {
	short_res, _ := client.SMembers(ID + ":<http://www.w3.org/2000/01/rdf-schema#comment>").Result()
	long_res, _ := client.SMembers(ID + ":<http://dbpedia.org/ontology/abstract>").Result()
	if len(short_res) > len(long_res) {
		return len(short_res)
	}
	return len(long_res)
}

func trimLabel(input string) string {
	return strings.TrimPrefix(strings.TrimSuffix(input, "\"@en"), "\"")
}

func trimImg( input string) string {
    return strings.TrimPrefix(strings.TrimSuffix(input, ">"), "<")
}

func UnescapeUnicode(input string) string {
    unicodeRE := regexp.MustCompile("\\\\u[a-fA-F0-9]{4}")
    return string(unicodeRE.ReplaceAllFunc([]byte(input), expandUnicodeRune))
}

func expandUnicodeRune(esc []byte) []byte {
  ri, _:= strconv.ParseInt(string(esc[2:]), 16, 32)
  r := rune(ri)
  repr := make([]byte, utf8.RuneLen(r))
  utf8.EncodeRune(repr, r)
  return repr
}
